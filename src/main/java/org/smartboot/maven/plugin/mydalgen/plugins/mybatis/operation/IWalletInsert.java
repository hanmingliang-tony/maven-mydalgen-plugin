/*
 * Taobao.com Inc.
 * Copyright (c) 2000-2004 All Rights Reserved.
 */
package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.operation;

import org.smartboot.maven.plugin.mydalgen.Table;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletColumn;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletOperation;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config.IWalletOperationConfig;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.util.DalUtil;

/**
 * @author Seer
 * @version IWalletInsert.java, v 0.1 2015年7月25日 上午10:25:27 Seer Exp.
 */
public class IWalletInsert extends IWalletOperation {

    public static final String OP_TYPE = "insert";

    /**
     * Constructor for IWalletInsert.
     */

    public IWalletInsert(IWalletOperationConfig opConfig) {

        super(opConfig);

        if (PARAM_TYPE_PRIMITIVE.equals(opConfig.getParamType())) {
            paramType = PARAM_TYPE_PRIMITIVE;
        } else {
            // default
            paramType = PARAM_TYPE_OBJECT;
        }

        multiplicity = MULTIPLICITY_ONE;
    }


    public String getReturnType() {
        if (getTable().getPkColumn() == null) {
            throw new IllegalStateException(getTable().getSqlName() + "无主键或多主键，可在table属性中指定虚拟主键dummypk。");
        } else {
            return ((IWalletColumn) getTable().getPkColumn()).getJavaType();
        }
    }

    /**
     * @return
     * @see org.smartboot.maven.plugin.mydalgen.plugins.mybatis.Operation#getTemplateSuffix()
     */
    public String getTemplateSuffix() {
        return OP_TYPE;
    }

    /**
     * @return
     */
    public String getMappedStatementType() {
        return OP_TYPE;
    }
}
