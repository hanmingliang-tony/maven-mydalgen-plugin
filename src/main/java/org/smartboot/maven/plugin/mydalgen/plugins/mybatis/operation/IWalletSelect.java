/*
 * Taobao.com Inc.
 * Copyright (c) 2000-2004 All Rights Reserved.
 */
package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.operation;

import org.apache.commons.lang.StringUtils;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletOperation;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletTable;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config.IWalletOperationConfig;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config.IWalletResultConfig;

import java.util.List;

/**
 * @author Seer
 * @version IWalletSelect.java, v 0.1 2015年7月25日 上午10:25:21 Seer Exp.
 */
public class IWalletSelect extends IWalletOperation {

    /**
     * default java type to return the result of multiple records
     */
    public static final String DEFAULT_MANY_RETURN_TYPE_NO_PAGING = "java.util.List";

    /**
     * default java type to return the result of multiple records with paging
     */
    public static final String OP_TYPE = "select";

    /**
     * Constructor for IWalletSelect.
     */
    public IWalletSelect(IWalletOperationConfig conf) {
        super(conf);

        if (PARAM_TYPE_OBJECT.equals(conf.getParamType())) {
            paramType = PARAM_TYPE_OBJECT;
        } else {
            // default
            paramType = PARAM_TYPE_PRIMITIVE;
        }

        if (MULTIPLICITY_MANY.equals(conf.getMultiplicity())) {
            multiplicity = MULTIPLICITY_MANY;
        } else {
            // default
            multiplicity = MULTIPLICITY_ONE;
        }
    }

    private String getReturnTypeOne() {
        if (opConfig.getResultMap() != null) {
            return getTable().getResultMap(opConfig.getResultMap()).getClassAttr();
        } else if (opConfig.getResultType() != null) {
            return opConfig.getResultType();
        } else {
            return getTable().getDOClassName();
        }
    }

    /**
     * @return
     */
    public String getReturnType() {
        IWalletResultConfig resultConfig = opConfig.getResult();
        //未显示指定result
        if (resultConfig == null) {
            resultConfig = new IWalletResultConfig();
            if (multiplicity.equals(MULTIPLICITY_MANY)) {
                resultConfig.setJavaType(List.class.getName());
                resultConfig.setGenericType(getTable().getDOClassName());
            } else {
                resultConfig.setJavaType(getTable().getDOClassName());
            }
        }

        String javaType = resultConfig.getJavaType();
        int index = javaType.lastIndexOf(".");
        if (index >= 0) {
            getTable().addDaoImport(javaType);
            javaType = javaType.substring(index + 1);
        }
        if (StringUtils.isNotBlank(resultConfig.getGenericType())) {
            return javaType + "<" + resultConfig.getGenericType() + ">";
        } else {
            return javaType;
        }
    }

    /**
     * @return
     * @see org.smartboot.maven.plugin.mydalgen.plugins.mybatis.Operation#getTemplateSuffix()
     */
    public String getTemplateSuffix() {
        return OP_TYPE;
    }

    /**
     * 增加泛型类的导入
     * <p>
     * 当方法返回类型为List时,我们将它转化为泛型,即返回List<???>类型;<br>
     * 此方法,就是针对这种情况,而增加对???类的导入<br>
     * 如果???类型为long,int,则不需要增加导入;
     * </p>
     * <p>
     * add by lejin,2009-7-31 下午03:14:27,
     *
     * @author lejin
     */
    private void addImprotForGenericType() {
        String itemType = opConfig.getResultType();
        if (!StringUtils.equals(itemType, "long") && !StringUtils.equals(itemType, "int")) {
            getTable().addDaoImport(itemType);
            getTable().addIbatisImport(itemType);
        }
    }

    /**
     * @return
     */
    public String getMappedStatementType() {
        return OP_TYPE;
    }

    /**
     * @return
     */
    public String getMappedStatementResult() {
        if (opConfig.getResultMap() != null) {
            return "resultMap=\"" + opConfig.getResultMap() + "\"";
        } else {
            String result = getReturnTypeOne();

            if (getTable().getQualifiedDOClassName().equals(result)) {
                return "resultMap=\"" + getTable().getResultMapId() + "\"";
            } else {
                return "resultType=\"" + result + "\"";
            }
        }
    }

}
