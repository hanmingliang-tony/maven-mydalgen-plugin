/*
 * Taobao.com Inc.
 * Copyright (c) 2000-2004 All Rights Reserved.
 */
package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.operation;

import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletOperation;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config.IWalletOperationConfig;

/**
 * @author Seer
 * @version IWalletUpdate.java, v 0.1 2015年7月25日 上午10:24:49 Seer Exp.
 */
public class IWalletUpdate extends IWalletOperation {

    public static final String OP_TYPE = "update";

    /**
     * Constructor for IWalletUpdate.
     */
    public IWalletUpdate(IWalletOperationConfig conf) {
        super(conf);

        if (PARAM_TYPE_OBJECT.equals(conf.getParamType())) {
            paramType = PARAM_TYPE_OBJECT;
        } else {
            // default
            paramType = PARAM_TYPE_PRIMITIVE;
        }

        // default
        multiplicity = MULTIPLICITY_ONE;
    }

    /**
     * @return
     */
    public String getReturnType() {
        return "int";
    }

    /**
     * @return
     * @see org.smartboot.maven.plugin.mydalgen.plugins.mybatis.Operation#getTemplateSuffix()
     */
    public String getTemplateSuffix() {
        return OP_TYPE;
    }

    /**
     * @return
     */
    public String getMappedStatementType() {
        return OP_TYPE;
    }

}
