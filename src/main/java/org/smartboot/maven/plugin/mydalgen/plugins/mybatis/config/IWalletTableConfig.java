package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Seer
 * @version IWalletTableConfig.java, v 0.1 2015年7月25日 上午10:25:58 Seer Exp.
 */
public class IWalletTableConfig {
    /**
     * the table name in database
     */
    private String sqlName;

    /**
     * mybatis自定义标签
     */
    private String mybatis;

    /**
     * a list of all configured operations
     */
    private List<IWalletOperationConfig> operations = new ArrayList<IWalletOperationConfig>();

    /**
     * a map of all column configuration
     */
    private Map<String, IWalletColumnConfig> columns = new HashMap<String, IWalletColumnConfig>();

    /**
     * a list of all result maps
     */
    private List<IWalletResultMapConfig> resultMaps = new ArrayList<IWalletResultMapConfig>();

    /**
     * Constructor for IWalletTableConfig.
     */
    public IWalletTableConfig() {
        super();
    }

    /**
     * @return
     */
    public String getSqlName() {
        return sqlName;
    }

    /**
     * @param string
     */
    public void setSqlName(String string) {
        sqlName = string;
    }

    public String getMybatis() {
        return mybatis;
    }

    public void addMybatis(String mybatis) {
        this.mybatis = mybatis;
    }

    /**
     * @return
     */
    public List<IWalletOperationConfig> getOperations() {
        return operations;
    }

    /**
     * Add an operation configuration to the operation list, and have the
     * operation points to this table configuration.
     *
     * @param operationConfig
     */
    public void addOperation(IWalletOperationConfig operationConfig) {
        operations.add(operationConfig);
        operationConfig.setTableConfig(this);
    }

    /**
     * Get a column configuration by its name.
     *
     * @param name
     * @return
     */
    public IWalletColumnConfig getColumn(String name) {
        return columns.get(name.toLowerCase());
    }

    /**
     * Add a column configuration.
     *
     * @param columnConfig
     */
    public void addColumn(IWalletColumnConfig columnConfig) {
        if (columnConfig != null) {
            columns.put(columnConfig.getName().toLowerCase(), columnConfig);
        }
    }

    /**
     * @return
     */
    public List<IWalletResultMapConfig> getResultMaps() {
        return resultMaps;
    }

    /**
     *
     */
    public void addResultMap(IWalletResultMapConfig resultMapConfig) {
        resultMaps.add(resultMapConfig);
        resultMapConfig.setTableConfig(this);
    }

}
