package org.smartboot.maven.plugin.mydalgen.plugins.mybatis.operation;

import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.IWalletOperation;
import org.smartboot.maven.plugin.mydalgen.plugins.mybatis.config.IWalletOperationConfig;

public class IWalletDelete extends IWalletOperation {

    public static final String OP_TYPE = "delete";

    /**
     * Constructor for IWalletDelete.
     */
    public IWalletDelete(IWalletOperationConfig conf) {
        super(conf);

        if (PARAM_TYPE_OBJECT.equals(conf.getParamType())) {
            paramType = PARAM_TYPE_OBJECT;
        } else {
            // default
            paramType = PARAM_TYPE_PRIMITIVE;
        }

        multiplicity = MULTIPLICITY_ONE;
    }

    /**
     * @return
     */
    public String getReturnType() {
        return "int";
    }

    /**
     * @return
     * @see org.smartboot.maven.plugin.mydalgen.plugins.mybatis.Operation#getTemplateSuffix()
     */
    public String getTemplateSuffix() {
        return OP_TYPE;
    }

    /**
     * @return
     */
    public String getMappedStatementType() {
        return OP_TYPE;
    }
}
